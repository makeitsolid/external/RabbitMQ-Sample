# RabbitMQ Sample

This project was setup to give a quick overview of what RabbitMQ is, and how it's integrated within C# (.NET).

## What this project does (and does not) provide
What you'll be able to do:
- Have a high level understanding of what RabbitMQ & Docker are/can do
- Be able to launch a RabbitMQ instance on your machine and connect to it through docker (containerized environments)
- Have a quick overview of how RabbitMQ works and integrates with C# (.NET)
- A fully working RabbitMQ Instance and C# console project setup in less than a minute.

What you won't be able to do:
- Understand what RabbitMQ FULLY is/can do
- Understand what Docker FULLY is/can do
- Save the world from hunger and war

## Prerequisites

What will you need:
- Docker Desktop - [Latest version - make use of Linux containers, no matter if you're on Windows or not](https://www.docker.com/get-started)
- .NET Core 3.1 SDK - [Latest LTS](https://dotnet.microsoft.com/download/visual-studio-sdks)

That's it.

## Definition

Basic definitions. Please search for more detailed information on your favorite search engine.

### Docker

Docker is an open platform for distributed applications and containers. Basically, with docker, you are able to launch unlimited (or at least as many as your hardware will allow) containers, which are **virtual machines** that are based on **images** that will be as limited in hardware, preparation and installation, as the end application you're wanting to launch needs. All information on docker website or on [this cheatsheet](https://www.jrebel.com/blog/docker-commands-cheat-sheet).

**Example:** You are developing a .NET webshop. You need a database, mailing, application server to be able to test it fully.

Through the classic way you'll have to temper with your computer, and all those risks (not exhaustive):
1. Break your own computer using the try-and-error methodology to setup your computer
1. Not able to launch something because of a OS dependency that is present on production, but not on your own computer
1. Not be able to replicate the production environment, which means whatever you'll test on your computer is not guaranteed to work on release
1. ...

Docker solves this by distributing images (which are opensourced, only use official/highly rated ones to avoid issues) that can be downloaded through **[docker commands](https://www.jrebel.com/blog/docker-commands-cheat-sheet)** or **[docker-compose](https://docs.docker.com/compose/gettingstarted/)** configuration files.

For the hereabove example you'll basically have:
1. One docker-compose file with all configuration in it
1. That's it.

That docker-compose file, when launched, will get all the different containers you need (1 for SQL Server, 1 web server nginx/apache/whatever you configured with all .NET dependencies).

### RabbitMQ

RabbitMQ is a lightwight and easy to deploy message broker: it accepts and forwards messages. It’s written in Erlang language. Basically it's a producer-consumer messaging system. [All TLDR information here](https://dev-pages.info/rabbitmq-cheat-sheets/)

- `Producer` - a programm that sends messages.
- `Consumer` - a programm that reads messages.
- `Exchange` - takes a message from producers and routes it into zero or more queues.
- `Bindings` - rules that exchanges use to route messages to queues.
- `Queue` - a buffer that stores messages.

What's good with a message broker; is that:
1. You can set it up to persist *or to not persist* messages after they have been consumed.
1. Depending on the **consuming mode** you're using, the ack/nack status system can be pretty reliable for locking preventio: consuming a message can be `ack` (acknowledged) or `nack` (not acknowledged). Acknowledged can be seen as a confirmation that the consumer was able to get the message and consume it further. Not acknowledged means that the consumer might or might not have been able to consume it correctly; so it's placed back in the queue.

## Configuration

The docker-compose file is ready for use. All configuration can be done in the file `docker/docker-compose.yml`

Default configuration:
- User: `rabbitmq`
- Password: `rabbitmq` 
- Connection port: `5672`
- Webviewer port: `15672`

## Usage

```bash
$ git clone https://gitlab.com/makeitsolid/internal/RabbitMQ-Sample.git
$ cd docker
$ docker-compose up -d
```

You'll then be able to open a browser on this link: [http://localhost:15672](http://localhost:15672) and login with here above credentials to create your first queue and try out the Producer & Consumer projects.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[Apache License 2.0](https://choosealicense.com/licenses/apache-2.0/)
Solution created by [Make IT Solid BV](https://makeitsolid.be).